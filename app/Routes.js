
import express from 'express';
import path from 'path';

let Routes = express.Router();

Routes.get('/', (req, res)=>{
	res.render('index');
});

export default Routes;
