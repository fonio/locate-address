
let config = {
	
	development: {

		debug: true,

		server: {
			port: 8090,
			keyPath: './locate-address.key',
			certPath: './locate-address_com.crt'	
		},

		session: {
		}

	},

	production: {

		debug: false,

		server: {
			port: 8445,
			keyPath: './locate-address.key',
			certPath: './locate-address_com.crt'			
		},

		session: {
		}

	}

};

export default config[process.env.NODE_ENV];
