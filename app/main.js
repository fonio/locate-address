import App from './App';

App.start().then(()=>{
	console.log(`Started on port ${App.server.address().port}`);
}).catch((error)=>{
	console.log(error);
});
