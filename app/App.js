import fs from 'fs';
import path from 'path';
import http from 'http';
import https from 'https';
import express from 'express';
import exphbs from 'express-handlebars';
import bodyParser from 'body-parser';
import Routes from './Routes';
import config from './config';
import session from 'express-session';
//import helpers from './app/views/helpers';

export default class App {

	static start(){

		return new Promise((resolve, reject) => {

			let server = new express();

			server.use(express.static('www'));
			
			server.set('trust proxy', 1);
			server.use(session({
				name: 'session',
				secret: 'iybg434g6gn3i43gn6i34lglohjv3l4jhvl3jk4b',
				resave: true,
				saveUninitialized: true,
				cookie: { 
					secure: true, 
					maxAge: config.session.maxAge
				},
			}));

			server.use(bodyParser.json({ type: 'application/json' }));
			server.use(bodyParser.urlencoded({ extended: false }));

			server.engine('hbs', exphbs({
				extname: '.hbs',
				defaultLayout: 'main',
				layoutsDir: './app/views/layouts/',
//				helpers,
				partialsDir: './app/views/partials'
			}));

			server.set('view engine', '.hbs');
			server.set('views', path.join(__dirname, 'views'));

			server.use(Routes);

			if(config.server.keyPath && config.server.certPath){
				this.server = https.createServer({
					passphrase: config.server.passphrase,
					key: fs.readFileSync(config.server.keyPath, 'utf8'),
					cert: fs.readFileSync(config.server.certPath, 'utf8')
				}, server);
			} else {
				this.server = http.createServer(server);
			}

			this.server.listen(config.server.port, (error)=>{
				if(error){
					reject(error);
				}

				resolve();
			});
		});
	}

	static close(){
		console.log('CLOSING');
		this.server.close();
	}
}
