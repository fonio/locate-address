export const EventSupportDecorator = (ChildClass)=>{

	class EventSupport extends ChildClass {

		constructor(){
			super(...arguments);
			this.listeners = {};
		}

		on(type, func){
			if(!this.listeners[type]){
				this.listeners[type] = [];
			}

			this.listeners[type].push(func);
		}

		fireEvent(event, details, originalEvent){
			event = typeof event === 'string' ? { type: event } : event;
			event.target = (!event.target) ? this : event.target;
			event.timestamp = new Date().getTime();
			event.details = details;

			if (!event.type){
				throw new Error('missing \'type\' property');
			}

			// attach original event
			if (typeof originalEvent !== 'undefined'){
				event.originalEvent = originalEvent;
			}

			// add details to event
			if (typeof details !== 'undefined'){
				for (let detail in details){
					if(details.hasOwnProperty(detail)){
						event[detail] = details[detail];
					}
				}
			}

			// call listeners
			let listeners = this.listeners[event.type];
			if(typeof listeners !== 'undefined') {
				for (let i=0, l=listeners.length; i < l; i++){
					let _details = event.details;
					delete event.details;

					try {
	
						listeners[i](this, event, _details);
					} catch (error) {
						console.log(error);
					}
				}
			}
		}		
	}

	return EventSupport;
}