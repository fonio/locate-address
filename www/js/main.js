import Map 			from './map/Map';
import Marker 		from './map/Marker';
import Geocoder 	from './map/Geocoder';

// expose object to the site
window['Site'] = { Map, Marker, Geocoder };
