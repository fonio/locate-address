import { EventSupportDecorator } from '../EventSupportDecorator';

export default class Map {

	constructor(opts){

		// google.maps.Map is not available on initial library load, hence extending here
		@EventSupportDecorator
		class Map extends google.maps.Map {
			get marker(){ return this._marker; }
			set marker(marker){ 
				if(this._marker){
					this._marker.setMap(null);
				}

				this._marker = marker;
				this._marker.setMap(this);

				this._marker.on('dragstart', ()=>{ this.fireEvent('marker-dragstart') });				
				this._marker.on('dragend', ()=>{ this.fireEvent('marker-dragend') });				
				this._marker.on('drag', ()=>{ this.fireEvent('marker-drag') });				
				this._marker.on('dblclick', ()=>{ this.fireEvent('marker-dblclick') });				
			}

			recenter(){
				this.panTo(this.marker ? this.marker.getPosition() : opts.location);
			}
		}

		this.instance = new Map(opts.container[0], {
			zoom: opts.zoom || 19,
			center: opts.location,
			streetViewControl: false,
			zoomControl: false,
			mapTypeControl: false,
			draggable: true,
			draggableCursor: 'crosshair',
	    	backgroundColor: '#c4d9d3',
			styles: [{"featureType":"administrative","elementType":"all","stylers":[{"visibility":"off"}]},{"featureType":"administrative","elementType":"geometry.stroke","stylers":[{"visibility":"on"}]},{"featureType":"administrative","elementType":"labels","stylers":[{"visibility":"on"},{"color":"#716464"},{"weight":"0.01"}]},{"featureType":"administrative.country","elementType":"labels","stylers":[{"visibility":"on"}]},{"featureType":"landscape","elementType":"all","stylers":[{"visibility":"simplified"}]},{"featureType":"landscape.natural","elementType":"geometry","stylers":[{"visibility":"simplified"}]},{"featureType":"landscape.natural.landcover","elementType":"geometry","stylers":[{"visibility":"simplified"}]},{"featureType":"poi","elementType":"all","stylers":[{"visibility":"simplified"}]},{"featureType":"poi","elementType":"geometry.fill","stylers":[{"visibility":"simplified"}]},{"featureType":"poi","elementType":"geometry.stroke","stylers":[{"visibility":"simplified"}]},{"featureType":"poi","elementType":"labels.text","stylers":[{"visibility":"simplified"}]},{"featureType":"poi","elementType":"labels.text.fill","stylers":[{"visibility":"simplified"}]},{"featureType":"poi","elementType":"labels.text.stroke","stylers":[{"visibility":"simplified"}]},{"featureType":"poi.attraction","elementType":"geometry","stylers":[{"visibility":"on"}]},{"featureType":"road","elementType":"all","stylers":[{"visibility":"on"}]},{"featureType":"road.highway","elementType":"all","stylers":[{"visibility":"off"}]},{"featureType":"road.highway","elementType":"geometry","stylers":[{"visibility":"on"}]},{"featureType":"road.highway","elementType":"geometry.fill","stylers":[{"visibility":"on"}]},{"featureType":"road.highway","elementType":"geometry.stroke","stylers":[{"visibility":"simplified"},{"color":"#a05519"},{"saturation":"-13"}]},{"featureType":"road.local","elementType":"all","stylers":[{"visibility":"on"}]},{"featureType":"transit","elementType":"all","stylers":[{"visibility":"simplified"}]},{"featureType":"transit","elementType":"geometry","stylers":[{"visibility":"simplified"}]},{"featureType":"transit.station","elementType":"geometry","stylers":[{"visibility":"on"}]},{"featureType":"water","elementType":"all","stylers":[{"visibility":"simplified"},{"color":"#84afa3"},{"lightness":52}]},{"featureType":"water","elementType":"geometry","stylers":[{"visibility":"on"}]},{"featureType":"water","elementType":"geometry.fill","stylers":[{"visibility":"on"}]}]
		});

		// events
		this.instance.addListener('click', (event)=>{ this.instance.fireEvent('click', {lat: event.latLng.lat(), lng: event.latLng.lng()}) });
		this.instance.addListener('dragstart', (event)=>{ this.instance.fireEvent('dragstart') });
		this.instance.addListener('drag', (event)=>{ this.instance.fireEvent('drag') });
		this.instance.addListener('dragend', (event)=>{ this.instance.fireEvent('dragend') });
		this.instance.addListener('zoom_changed', (event)=>{ this.instance.fireEvent('zoom-changed') });

		return this.instance;
	}

}
