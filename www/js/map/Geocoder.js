
export default class Geocoder {

	constructor(){
		class Geocoder extends google.maps.Geocoder {
		}

		this.instance = new Geocoder();
	}

	geocode(latlng){
		return new Promise((resolve, reject)=>{
			this.instance.geocode({ location: latlng }, (results, status)=>{
				if(status === "OK"){
					resolve(results, status);
				} else {
					reject(results, status);
				}
			});
		});
	}

}
