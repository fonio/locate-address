import { EventSupportDecorator } from '../EventSupportDecorator';

export default class Marker {

	constructor(latlng){

		@EventSupportDecorator
		class GMarker extends google.maps.Marker {
		}

		let gmarker = new GMarker({
			position: latlng,
			draggable: true,
			icon: {
				path: 'M25.015 2.4c-7.8 0-14.121 6.204-14.121 13.854 0 7.652 14.121 32.746 14.121 32.746s14.122-25.094 14.122-32.746c0-7.65-6.325-13.854-14.122-13.854z',
				fillOpacity: 1,
				fillColor: '#D43D38',
				strokeColor: '#fff',
				strokeWeight: 2,
				anchor: new google.maps.Point(25, 48),
				scale: isMobile ? 1.5 : 1
			}
		});

		// pass the click event up using event support
		gmarker.addListener('click', ()=>{ gmarker.fireEvent('click') });
		gmarker.addListener('dblclick', ()=>{ gmarker.fireEvent('dblclick') });
		gmarker.addListener('dragstart', ()=>{ gmarker.fireEvent('dragstart') });
		gmarker.addListener('drag', ()=>{ gmarker.fireEvent('drag') });
		gmarker.addListener('dragend', ()=>{ gmarker.fireEvent('dragend') });

		return gmarker;
	}

}

